FROM golang:1.19.1-alpine3.16 AS builder
RUN apk --no-cache add ca-certificates make git && update-ca-certificates

COPY . /release-cli
WORKDIR /release-cli

RUN make build

FROM curlimages/curl:8.1.0

COPY --from=builder /release-cli/bin/release-cli /usr/local/bin/release-cli
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
