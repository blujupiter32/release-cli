package app_test

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	testlog "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/release-cli/internal/app"
	"gitlab.com/gitlab-org/release-cli/internal/testdata"
	"gitlab.com/gitlab-org/release-cli/internal/testhelpers"
)

var unknownAuthorityErrMsg = (x509.UnknownAuthorityError{}).Error()

func TestApp(t *testing.T) {
	chdirSet := false

	chdir := testhelpers.ChdirInPath(t, "../", &chdirSet)
	defer chdir()

	tests := map[string]struct {
		res            int
		args           []string
		wantErrStr     string
		wantLogEntries []string
	}{
		"create_success": {
			res:            testdata.ResponseCreateReleaseSuccess,
			args:           []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0"},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		"get_success": {
			res:            testdata.ResponseGetReleaseSuccess,
			args:           []string{"get", "--tag-name", "v0.1"},
			wantLogEntries: []string{"Getting release"},
		},
		"success_with_description_file": {
			res:            testdata.ResponseCreateReleaseSuccess,
			args:           []string{"create", "--name", "release name", "--description", "testdata/description.txt", "--tag-name", "v1.1.0"},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		"success_with_description_single_word": {
			res:            testdata.ResponseCreateReleaseSuccess,
			args:           []string{"create", "--name", "release name", "--description", "description", "--tag-name", "v1.1.0"},
			wantLogEntries: []string{"Creating Release...", "file does not exist, using string value for --description", "release created successfully!"},
		},
		"success_with_assets_link_json": {
			res: testdata.ResponseCreateReleaseSuccess,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--assets-link", `{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy1"}`},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		"success_with_array_of_assets_link_json": {
			res: testdata.ResponseCreateReleaseSuccess,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--assets-link", `[{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy1"}, {"name":"Asset2","url":"https://<domain>/some/location/2","link_type":"other","filepath":"xzy2"}]`},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		"success_with_one_asset_and_array_of_assets_link_json": {
			res: testdata.ResponseCreateReleaseSuccess,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--assets-link", `[{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy1"}, {"name":"Asset2","url":"https://<domain>/some/location/2","link_type":"other","filepath":"xzy2"}]`,
				"--assets-link", `{"name":"Asset3","url":"https://<domain>/some/location/3","link_type":"other","filepath":"xzy3"}`},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		"failed_with_assets_not_json": {
			res: testdata.ResponseCreateReleaseSuccess,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--assets-link", "not_json"},
			wantErrStr:     "new CreateReleaseRequest: failed to parse assets: 1 error occurred:\n\t* invalid delimiter for asset: \"not_json\"\n\n",
			wantLogEntries: []string{"Creating Release..."},
		},
		"failed_with_one_good_asset_and_one_asset_not_json": {
			res: testdata.ResponseCreateReleaseSuccess,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--assets-link", `{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy1"}`,
				"--assets-link", "not_json"},
			wantErrStr:     "new CreateReleaseRequest: failed to parse assets: 1 error occurred:\n\t* invalid delimiter for asset: \"not_json\"\n\n",
			wantLogEntries: []string{"Creating Release..."},
		},
		"success_with_milestones_and_released_at": {
			res: testdata.ResponseCreateReleaseSuccess,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--assets-link", `{"name":"asset name 1","url":"url 1"}`,
				"--milestone", "v1.0", "--milestone", "v1.0-rc", "--released-at", "2019-01-03T01:55:18.203Z"},
			wantErrStr:     "",
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		"failed_with_unknown_milestone": {
			res: testdata.ResponseMilestoneNotFound,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--milestone", "unknown"},
			wantErrStr:     "failed to create release: API Error Response status_code: 400 message: Milestone(s) not found: unknown",
			wantLogEntries: []string{"Creating Release..."},
		},
		"failed_with_invalid_released_at": {
			res: testdata.ResponseCreateReleaseSuccess,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--released-at", "2019/01/03"},
			wantErrStr:     "new CreateReleaseRequest: failed to parse released-at: parsing time \"2019/01/03\" as \"2006-01-02T15:04:05Z07:00\": cannot parse \"/01/03\" as \"-\"",
			wantLogEntries: []string{"Creating Release..."},
		},
		"create_invalid_tag": {
			res:            testdata.ResponseBadRequest,
			args:           []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "%invalid%_char-!ters"},
			wantErrStr:     "failed to create release: API Error Response status_code: 400 message: tag_name is missing",
			wantLogEntries: []string{"Creating Release..."},
		},
		"create_unauthorized": {
			res:            testdata.ResponseUnauthorized,
			args:           []string{"create", "--name", "release name", "--description", "release description", "--tag-name", t.Name(), "--ref", t.Name()},
			wantErrStr:     "failed to create release: API Error Response status_code: 401 message: 401 Unauthorized",
			wantLogEntries: []string{"Creating Release..."},
		},
		"create_forbidden": {
			res:            testdata.ResponseForbidden,
			args:           []string{"create", "--name", "release name", "--description", "", "--tag-name", t.Name(), "--ref", "12345"},
			wantErrStr:     "failed to create release: API Error Response status_code: 403 message: 403 Forbidden",
			wantLogEntries: []string{"Creating Release..."},
		},
		"internal_server_error": {
			res:            testdata.ResponseInternalError,
			args:           []string{"create", "--name", "release name", "--description", "release description", "--tag-name", t.Name(), "--ref", t.Name()},
			wantErrStr:     "failed to create release: API Error Response status_code: 500 message: 500 Internal Server Error",
			wantLogEntries: []string{"Creating Release..."},
		},
		"unexpected_error": {
			res:            testdata.ResponseUnexpectedError,
			args:           []string{"create", "--name", "release name", "--description", "release description", "--tag-name", t.Name(), "--ref", t.Name()},
			wantErrStr:     "failed to create release: API Error Response status_code: 500 message:  error: Something went wrong",
			wantLogEntries: []string{"Creating Release..."},
		},
		"get_error_response": {
			res:            testdata.ResponseInternalError,
			args:           []string{"get", "--tag-name", "v0.1"},
			wantErrStr:     "get release: API Error Response status_code: 500 message: 500 Internal Server Error",
			wantLogEntries: []string{"Getting release"},
		},
		"get_failed_missing_tag_name": {
			res:        testdata.ResponseInternalError,
			args:       []string{"get"},
			wantErrStr: "Required flag \"tag-name\" not set",
		},
		"success_with_release_file": {
			res:            testdata.ResponseCreateReleaseSuccess,
			args:           []string{"create-from-file", "--file", "testdata/release.yml"},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			log, hook := testlog.NewNullLogger()

			s := httptest.NewServer(handler(t, tt.res))
			defer s.Close()

			testApp := app.New(logrus.NewEntry(log), t.Name())
			args := []string{"release-cli", "--server-url", s.URL, "--job-token", "token", "--project-id", "projectID"}
			args = append(args, tt.args...)
			err := testApp.Run(args)
			if tt.wantErrStr != "" {
				require.EqualError(t, err, tt.wantErrStr)
			} else {
				require.NoError(t, err)
			}

			require.ElementsMatch(t, toStrSlice(t, hook.AllEntries()), tt.wantLogEntries)
		})
	}
}

func TestClientTimeout(t *testing.T) {
	log, hook := testlog.NewNullLogger()
	timeout := 1 * time.Millisecond

	s := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			time.Sleep(timeout * 3)
			w.WriteHeader(http.StatusNoContent)
		}))
	defer s.Close()

	testApp := app.New(logrus.NewEntry(log), t.Name())
	args := []string{"release-cli", "--server-url", s.URL, "--job-token",
		"token", "--project-id", "projectID", "--timeout", timeout.String(),
		"create", "--name", "release name", "--description",
		"release description", "--tag-name", "v1.1.0",
	}

	err := testApp.Run(args)
	require.Error(t, err)

	var urlErr *url.Error

	require.True(t, errors.As(err, &urlErr), "err must be url.Error")
	require.True(t, urlErr.Timeout(), "err must be a timeout")
	require.ElementsMatch(t, toStrSlice(t, hook.AllEntries()), []string{"Creating Release..."})
}

func TestHTTPSCustomCA(t *testing.T) {
	chdirSet := false

	chdir := testhelpers.ChdirInPath(t, "../", &chdirSet)
	defer chdir()

	s := newUnstartedTLSServer(t, "testdata/certs/localhost.pem", "testdata/certs/localhost.key", handler(t, testdata.ResponseCreateReleaseSuccess))

	s.StartTLS()
	defer s.Close()

	tests := []struct {
		name           string
		certFlags      []string
		env            string
		wantErrStr     string
		wantLogEntries []string
	}{
		{
			name:           "without_certificate_authority",
			wantErrStr:     unknownAuthorityErrMsg,
			wantLogEntries: []string{"Creating Release..."},
		},
		{
			name:           "with_insecure_https_success",
			certFlags:      []string{"--insecure-https"},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		{
			name:           "with_valid_custom_ca_as_file_name",
			certFlags:      []string{"--additional-ca-cert-bundle", "testdata/certs/CA.pem"},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		{
			name: "with_valid_custom_ca_from_flag_as_cert_content",
			certFlags: func() []string {
				f, err := os.ReadFile("testdata/certs/CA.pem")
				require.NoError(t, err)

				return []string{"--additional-ca-cert-bundle", string(f)}
			}(),
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		{
			name:           "with_valid_custom_ca_from_env_as_file_name",
			env:            "testdata/certs/CA.pem",
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		{
			name: "with_valid_custom_ca_from_env_as_cert_content",
			env: func() string {
				f, err := os.ReadFile("testdata/certs/CA.pem")
				require.NoError(t, err)

				return string(f)
			}(),
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		{
			name:           "with_invalid_custom_ca_as_file_name",
			certFlags:      []string{"--additional-ca-cert-bundle", "testdata/certs/CA_invalid.pem"},
			wantErrStr:     unknownAuthorityErrMsg,
			wantLogEntries: []string{"Creating Release...", "No certs appended, using system certs only"},
		},
		{
			name: "with_invalid_custom_ca_from_flag_as_cert_content",
			certFlags: func() []string {
				f, err := os.ReadFile("testdata/certs/CA_invalid.pem")
				require.NoError(t, err)

				return []string{"--additional-ca-cert-bundle", string(f)}
			}(),
			wantErrStr:     unknownAuthorityErrMsg,
			wantLogEntries: []string{"Creating Release...", "No certs appended, using system certs only"},
		},

		{
			name:           "with_invalid_custom_ca_from_env_as_file_name",
			env:            "testdata/certs/CA_invalid.pem",
			wantErrStr:     unknownAuthorityErrMsg,
			wantLogEntries: []string{"Creating Release...", "No certs appended, using system certs only"},
		},
		{
			name: "with_invalid_custom_ca_from_env_as_cert_content",
			env: func() string {
				f, err := os.ReadFile("testdata/certs/CA_invalid.pem")
				require.NoError(t, err)

				return string(f)
			}(),
			wantErrStr:     unknownAuthorityErrMsg,
			wantLogEntries: []string{"Creating Release...", "No certs appended, using system certs only"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			log, hook := testlog.NewNullLogger()

			err := os.Setenv("ADDITIONAL_CA_CERT_BUNDLE", tt.env)
			require.NoError(t, err)

			testApp := app.New(logrus.NewEntry(log), t.Name())
			args := []string{"release-cli", "--server-url", s.URL, "--job-token",
				"token", "--project-id", "projectID"}

			args = append(args, tt.certFlags...)

			args = append(args, "create", "--name", "release name", "--description",
				"release description", "--tag-name", "v1.1.0")

			err = testApp.Run(args)
			if tt.wantErrStr != "" {
				require.Error(t, err)
				require.Contains(t, err.Error(), tt.wantErrStr)
			} else {
				require.NoError(t, err)
			}

			require.ElementsMatch(t, toStrSlice(t, hook.AllEntries()), tt.wantLogEntries)
		})
	}
}

func toStrSlice(t *testing.T, entries []*logrus.Entry) []string {
	t.Helper()

	all := make([]string, len(entries))
	for k, e := range entries {
		all[k] = e.Message
	}

	return all
}

func handler(t *testing.T, i int) http.HandlerFunc {
	t.Helper()

	return func(w http.ResponseWriter, r *http.Request) {
		res := testdata.Responses[i]()
		defer require.NoError(t, res.Body.Close())

		body, err := io.ReadAll(res.Body)
		require.NoError(t, err)

		w.Header().Set("content-type", "application/json")
		w.WriteHeader(res.StatusCode)
		_, err = w.Write(body)
		require.NoError(t, err)
	}
}

func newUnstartedTLSServer(t *testing.T, cert, key string, handler http.Handler) *httptest.Server {
	t.Helper()

	server := httptest.NewUnstartedServer(handler)
	certificate, err := tls.LoadX509KeyPair(cert, key)
	require.NoError(t, err)

	rootCA := x509.NewCertPool()
	rootCA.AppendCertsFromPEM([]byte(cert))

	server.TLS = &tls.Config{
		MinVersion:   tls.VersionTLS12,
		Certificates: []tls.Certificate{certificate},
		RootCAs:      rootCA,
	}

	return server
}
