package gitlab

import (
	"context"
	"testing"

	"github.com/sirupsen/logrus"
	testlog "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/require"
)

func TestClient_request(t *testing.T) {
	tests := []struct {
		name         string
		method       string
		baseURL      string
		jobToken     string
		privateToken string
		projectID    string
		wantErrMsg   string
	}{
		{
			name:      "no_error_job_token",
			baseURL:   "http://127.0.0.1",
			jobToken:  "job-token",
			projectID: "projectID",
		},
		{
			name:         "no_error_private_token",
			baseURL:      "http://127.0.0.1",
			privateToken: "private-token",
			projectID:    "projectID",
		},
		{
			name:         "private_token_over_job_token",
			baseURL:      "http://127.0.0.1",
			jobToken:     "job-token",
			privateToken: "private-token",
			projectID:    "projectID",
		},
		{
			name:         "no_token_provided",
			baseURL:      "http://127.0.0.1",
			jobToken:     "",
			privateToken: "",
			projectID:    "projectID",
			wantErrMsg:   errMissingToken.Error(),
		},
		{
			name:       "invalid_url",
			baseURL:    "%",
			jobToken:   "job-token",
			wantErrMsg: "invalid URL escape \"%\"",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			log, _ := testlog.NewNullLogger()
			gc, err := New(tt.baseURL, tt.jobToken, tt.privateToken, tt.projectID, &MockHTTPClient{}, logrus.NewEntry(log))
			if tt.wantErrMsg != "" {
				require.NotNil(t, err)
				require.Contains(t, err.Error(), tt.wantErrMsg)
				return
			}
			require.NoError(t, err)

			got, err := gc.request(context.Background(), tt.method, tt.baseURL, nil)
			require.NoError(t, err)

			require.NotNil(t, got)

			if tt.jobToken != "" && tt.privateToken != "" || tt.privateToken != "" {
				require.Equal(t, got.Header.Get("PRIVATE-TOKEN"), tt.privateToken)
			} else if tt.jobToken != "" {
				require.Equal(t, got.Header.Get("JOB-TOKEN"), tt.jobToken)
			}

			require.Equal(t, got.Header.Get("Content-Type"), "application/json")
			require.Equal(t, got.Header.Get("User-Agent"), customUserAgent)
		})
	}
}
